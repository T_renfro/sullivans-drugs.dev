<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_sd');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|o8tCCbt9a:44k;[(6t_pAoZCO(_l/:u1fgCiHj#]dKjojC?:<39<ch-wF?D-@Xs');
define('SECURE_AUTH_KEY',  'z-7XszMdz5AB#,{fH[BAKp+p|N,+!#je}-<:<Uu+0M}veJjBXB$Gp0fmO+ev6GPm');
define('LOGGED_IN_KEY',    '-%[7lo7Z;G]RV,Rv+a;,t1ldN%&L:U[@1<*J TkET)JfvXrGLYAVcCnyol<uH!1-');
define('NONCE_KEY',        'QM[vU<M&H;+H&?s;uQfO5U6z.k9}^E6MI+x3S?rhs_6{t?GHSiVY83-SLkT85](#');
define('AUTH_SALT',        'z%eR^},<-[YVbE?=N-%<u_9i!zb}PM1Y>yg 9^rkk,Cpcz:BmTx`_}4Ea*D(?W4G');
define('SECURE_AUTH_SALT', '|@*ti#U|?#Kx~xiSXK..n)k:;L{o14b4C<]|vAHut|#-Um!K[j)9^ryZT w>RBoe');
define('LOGGED_IN_SALT',   '{d=aJsy-hG3+>+&~EF%A9(ci c]>%lzf+xQ>|+ZN*)5cQh;MdcCgHej>f}@nz3or');
define('NONCE_SALT',       'BT`U9-8b8k7pDf4)awq|K5Nk6gMW7};YU,NU]O2;t3kc%=*^M#8Eqa<8AO}:Q^Z4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
