<?php
/**
 * Template Name: Location Page
 *
 * @package small-business-theme
 *
 */

get_header(); 

get_template_part( 'templates/hero-section' );
get_template_part( 'templates/location-featured-services');
get_template_part( 'templates/location-page' );
?>

<?php get_footer(); ?>