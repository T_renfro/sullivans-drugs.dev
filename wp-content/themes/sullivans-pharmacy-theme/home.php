<?php
/**
 * Template Name: Home Page
 *
 * @package small-business-theme
 *
 */

get_header(); 

get_template_part( 'templates/hero-section' );
get_template_part( 'templates/services-section' );
get_template_part( 'templates/about-section' );
get_template_part( 'templates/location-section' );

?>

<?php get_footer(); ?>