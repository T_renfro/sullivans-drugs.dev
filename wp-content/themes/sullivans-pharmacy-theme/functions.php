<?php

/**
 *
 * @package small-business-theme
 *
 */

// Custom Function to Include Favicon
function favicon_link() {
	echo '<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />' . "\n";
}
add_action( 'wp_head', 'favicon_link');
add_theme_support( 'menus' );

//navigation
function register_my_menu() {
	 register_nav_menus( array(
	'header-menu' => 'Main Menu',
	'location-menu' => 'Location Menu',
) );
}
add_action( 'init', 'register_my_menu' );
