<div class="container">
	<div class="row">
		<div class="col-sm-12 bump-top-sm">
			<div class="row">
				<div class="col-xs-12 bump-bottom-sm">				
					<h2 class="location-featured-service-title center"><?php echo the_field('featured_service_title'); ?></h2>			
				</div>
			</div>
			<div class="row">

				<?php if( have_rows('featured_service_repeater') ): ?>
						<?php while( have_rows('featured_service_repeater') ): the_row();

						$serviceName           = get_sub_field('featured_service_title');
			            $serviceLink           = get_sub_field('featured_service_link');
			            $serviceImage          = get_sub_field('featured_service_icon');
			            $buttonText            = get_sub_field('featured_service_button_text');
			            $serviceImage          = get_sub_field('featured_service_image');
			            
			            ?>

					       	<div class="col-xs-12 col-sm-6">
					       		<div class="ui segment featured-services-wrapper">
					       			<div class="row">					       							       				
						       			<div class="col-sm-6 col-md-5">
						       				<img src="<?php echo $serviceImage ?>" class="img-responsive">						       				
											<label class="small"><?php echo $serviceName ?></label>
										</div>
										<div class="col-sm-6 col-md-6 col-md-offset-1">
						            		<a href="<?php echo $serviceLink ?>" class="pull-right ui button inverted red button featured-service-button"><i class="<?php echo get_sub_field('featured_service_icon'); ?> icon"></i><?php echo $buttonText ?></a>			            	
						            	</div>							            
							        </div>
					            </div>
					        </div>
						<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>	
</div>