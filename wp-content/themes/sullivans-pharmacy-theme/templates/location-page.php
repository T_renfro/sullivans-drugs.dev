<section class="location-page-wrapper">
	<div class="container">
		<?php if( have_rows('location') ): ?>
			<?php while( have_rows('location') ): the_row();

	                        // vars
			$storePhoto          = get_sub_field('store_photo_images');
			$locationName        = get_sub_field('location_name');
			$phoneNumber         = get_sub_field('phone_number');
			$faxNumber      	 = get_sub_field('fax_number');
			$locationAdress      = get_sub_field('location_address');
			$locationCity        = get_sub_field('location_city');
			$locationState       = get_sub_field('location_state');
			$locationZip         = get_sub_field('location_zip');
			$locationMapsLink    = get_sub_field('location_maps_link');
			$monFriOpen          = get_sub_field('mon-fri_Open');
			$monFriClose         = get_sub_field('mon-fri_close');
			$thursOpen	      	 = get_sub_field ('thursday_open');
			$thursClose			 = get_sub_field ('thursday_close');                 
			$satOpen             = get_sub_field('sat_open');
			$satClose            = get_sub_field('sat_close');
			$sunOpen             = get_sub_field('sun_open');
			$sunClose            = get_sub_field('sun_close');
			$locationServiceName = get_sub_field('location_service_name');
			$hoursWarning        = get_sub_field('hours_warning');
			$hoursWarningTwo     = get_sub_field('hours_warning_two');
			$facebookLike        = get_sub_field('facebook_like');
			$anchorID            = get_sub_field('anchor_id');
			?>
			<div class="row">                       	                
				<div class="col-xs-12 col-md-5">
					<div class="location-anchor-wrapper" id="<?php echo $anchorID; ?>">
						<div class="ui segment">
							<img class="location-image" src="<?php echo $storePhoto; ?>">								
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-7">
					<!-- Locations Panel -->
					<div class="ui segment set-min-height location-detail-panel">
						<div class="row">
							<div class="col-md-12">
								<ul class="list-inline location-name-direction">
									<li class="store-location-name"><h3 class="store-location-name"><?php echo $locationName; ?></h3></li>
									<li class="visible-xs pull-right"><div class=" fb-like" data-href="<?php echo $facebookLike; ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>

									<li class="hidden-xs">
										<label><strong>Phone</strong></label>
										<p class="store-location-number"> <?php echo $phoneNumber; ?></p>
									</li>
									<li class="hidden-xs">
										<label><strong>Fax</strong></label>
										<p class="store-location-number"><?php echo $faxNumber; ?></p>
									</li>
								</ul>
								<hr>
								<div class="visible-xs">
									<div class="bump-bottom-md">
										<label class="sm-text"><strong>Call Us</strong></label>
										<span class="sm-text pull-right"><a class="ui small red button" href="tel:<?php echo $phoneNumber; ?>"><?php echo $phoneNumber; ?></a></span>
									</div>
									<hr>
									<div class="nudge-bottom-sm sm-text"><a href="<?php echo $locationMapsLink; ?>">Get Directions &rarr;</a></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<ul class="location-address-list unstyled-list">
									<li><i class="marker icon"></i><strong>Address</strong></li>
									<li><?php echo $locationAdress; ?></li>
									<li><?php echo $locationCity; ?>, <?php echo $locationState; ?></li>													
									<li><?php echo $locationZip; ?></li>
									<li class="hidden-xs nudge-top-bottom-xs"><a href="<?php echo $locationMapsLink; ?>">Get Directions &rarr;</a></li>
									<li><div class="hidden-xs fb-like" data-href="<?php echo $facebookLike; ?>" data-layout="box_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
								</ul>
							</div>
							<div class="visible-xs">
								<div class="bump-bottom-md">
								</div>
							</div>

							<?php if ( get_sub_field( 'thursday_open' ) ): ?>

								<div class="col-md-4">
									<ul class="location-hours-list unstyled-list">
										<li><i class="wait icon"></i><strong>Hours</strong></li>
										<li class="faux-sc"><strong>M-T-W-F</strong></li>
										<li class="warning-text"><?php echo $hoursWarning; ?></li>
										<li><?php echo $monFriOpen; ?>&ndash;<?php echo $monFriClose; ?></li>
										<li class="faux-sc"><strong>Thursday</strong></li>
										<li><?php echo $thursOpen; ?>&ndash;<?php echo $thursClose; ?></li>															
										<li class="faux-sc"><strong>Saturday</strong></li>
										<li><?php echo $satOpen; ?>&ndash;<?php echo $satClose; ?></li>
										<li class="faux-sc"><strong>Sunday</strong></li>
										<li><?php echo $sunOpen; ?></li>
									</ul>
								</div>

							<?php elseif (get_sub_field( 'hours_warning_two') ): ?>
								<div class="col-md-4">
									<ul class="location-hours-list unstyled-list">
										<li><i class="wait icon"></i><strong>Hours</strong></li>
										<li class="faux-sc"><strong>Monday&ndash;Friday</strong></li>
										<li class="warning-text"><?php echo $hoursWarning; ?></li>																				
										<li><?php echo $monFriOpen; ?>&ndash;<?php echo $monFriClose; ?></li>
										<li class="warning-text"><?php echo $hoursWarningTwo; ?></li>																						
										<li class="faux-sc"><strong>Saturday</strong></li>
										<li class="warning-text"><?php echo $hoursWarning; ?></li>
										<li><?php echo $satOpen; ?>&ndash;<?php echo $satClose; ?></li>
										<li class="faux-sc"><strong>Sunday</strong></li>										
										<li><?php echo $sunOpen; ?><?php if ( get_sub_field( 'sun_close' ) ): ?>&ndash;<?php echo $sunClose; ?> <?php endif; ?></li>
									</ul>
								</div>
							<?php else: ?>

								<div class="col-md-4">
									<ul class="location-hours-list unstyled-list">
										<li><i class="wait icon"></i><strong>Hours</strong></li>
										<li class="faux-sc"><strong>Monday&ndash;Friday</strong></li>
										<?php if ( get_sub_field( 'hours_warning_two' ) ): ?><li class="warning-text"><?php echo $hoursWarningTwo; ?></li><?php endif; ?>
										<li><?php echo $monFriOpen; ?>&ndash;<?php echo $monFriClose; ?></li>												
										<li class="faux-sc"><strong>Saturday</strong></li>
										<li><?php echo $satOpen; ?>&ndash;<?php echo $satClose; ?></li>
										<li class="faux-sc"><strong>Sunday</strong></li>
										<li class="warning-text"><?php echo $hoursWarning; ?></li>
										<li><?php echo $sunOpen; ?><?php if ( get_sub_field( 'sun_close' ) ): ?>&ndash;<?php echo $sunClose; ?> <?php endif; ?></li>
									</ul>
								</div>



							<?php endif; // end of if field_name logic ?>
							<div class="visible-xs">
								<div class="bump-bottom-md">
								</div>
							</div>

							<div class="col-md-5">
								<ul class="location-services-list unstyled-list">
									<li><i class="treatment icon"></i><strong>Services</strong></li>
									<li><a a class="location-card-green-link" href="<?php echo get_sub_field('prescription_refill_service_link'); ?>"><?php echo get_sub_field('prescription_refill_service'); ?> &rarr;</a></li>	
									<?php 
									if( have_rows('location_services') ): ?>											
									<?php 

														// loop through rows (sub repeater)
									while( have_rows('location_services') ): the_row();
									?>

									<?php get_sub_field('location_services')?>
									<li><?php echo get_sub_field('location_service_name'); ?></li>										
								<?php endwhile; ?>
								<?php if ( get_sub_field( 'photo_service' ) ): ?>
									<li>
										<a class="location-card-green-link" href="<?php echo get_sub_field('photo_service_link'); ?>"><?php echo get_sub_field('photo_service'); ?> &rarr;</a>
									</li>
								<?php endif; // end of if field_name logic ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>		
	<?php endif; //if( get_sub_field('items') ): ?>																					
<?php endwhile; ?>
<?php endif; ?>
</div>
</section>