<div class="ui segment notop nobottom">
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1 bump-bottom-md">
					<h2 class="center"><?php the_field('service_headline'); ?></h2>
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="bump-bottom-md">
					<div class="col-xs-6">					
							<a class="visible-xs" href="<?php the_field('featured_service_appstore_link'); ?>"><img class="img-responsive app-store-img" src="<?php the_field('featured_service_appstore_image'); ?>"></a>
					</div>
					<div class="col-xs-6">
						<a class="visible-xs" href="<?php the_field('featured_service_appstore_link_android'); ?>"><img class="img-responsive app-store-img" src="<?php the_field('featured_service_appstore_image_android'); ?>"></a>
					</div>					
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-4 col-sm-offset-1">
					<img class="hidden-xs img-responsive" src="<?php the_field('featured_service_image'); ?>">
				</div>
				<div class="col-xs-12 col-sm-6">
					<h3 class="featured-service-title"><?php the_field('featured_service_title'); ?></h3>					
					<p class="featured-service-description"><?php the_field('featured_service_description'); ?></p>
					<ul class="unstyled-list list-inline bump-top-sm">
						<?php while(has_sub_field('featured_service_feature')):?>
							<li class="service-icon xs-text-desktop"><i class="circular <?php the_sub_field('featured_service_icon'); ?> icon"></i></li>
							<li class="sm-text nudge-right-sm"><?php the_sub_field('featured_service_label'); ?></li>
							<br class="visible-xs">
						<?php endwhile; // end of the loop. ?>	
					</ul>
					<div class="bump-top-md">
						<a href="<?php the_field('featured_service_link'); ?>"><button class="ui large inverted red button "><?php the_field('featured_service_cta'); ?></button></a>						
					</div>					
				</div>
			</div>			
			<hr>
			<div class="row">
				<?php if( have_rows('service') ): ?>
					<?php while( have_rows('service') ): the_row();

		                        // vars
					$serviceImage         = get_sub_field('service_image');
					$serviceName          = get_sub_field('service_name');
					$serviceDescription   = get_sub_field('service_description');
					$serviceCta           = get_sub_field('service_cta');
					$serviceLink          = get_sub_field('service_link');
					?>

					<div class="col-xs-12 col-sm-4 center">
						<img class="img-responsive img-center nudge-left-right-sm" src="<?php echo $serviceImage; ?>">
						<h4 class="primary-text center"><?php echo $serviceName; ?></h4>                                
						<p class="nudge-left-right-sm"><?php echo $serviceDescription; ?></p>
						<a class="xs-text-desktop" href="<?php echo $serviceLink ?>"><?php echo $serviceCta ?></a>
					</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
</div>