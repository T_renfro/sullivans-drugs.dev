<section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<h3 class="about-section-header"><strong><?php the_field('location_headline'); ?></strong></h3>
				<hr>
				<p class="footer-description"><?php the_field('location_description'); ?></p>
				<div class="bump-top-bottom-md">
					<a class="hidden-xs ui large inverted red button" href="<?php the_field('location_link'); ?>"><?php the_field('location_cta'); ?></a>
					<a class="visible-xs fluid ui large inverted red button" href="<?php the_field('location_link'); ?>"><?php the_field('location_cta'); ?></a>

				</div>
			</div>	
			<div class="col-xs-12 col-sm-5 col-sm-offset-1 hidden-xs">
				<div class="ui segment piled">
					<img class="img-responsive location-image" src="<?php the_field('location_image'); ?>">
				</div>
			</div>				
		</div>
	</div>
</section>