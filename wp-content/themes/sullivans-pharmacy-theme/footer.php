<?php
/**
 *
 * @package small-business-theme
 */
?>
<div class="footer-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-push-8">
				<div class="location-wrapper nudge-bottom-md">
					<h4 class="footer-location-header footer-header">Locations</h4>
					<hr>
					<div class="row">
						<div class="col-xs-6">
						<?php if( have_rows('footer_locations') ): ?>
					<?php while( have_rows('footer_locations') ): the_row();

		           					$footerLocationNameLeft  = get_sub_field('footer_location_name_left');
			                        $footerLocationNameRight  = get_sub_field('footer_location_name_right');
			                        $footerLocationLinkLeft = get_sub_field('footer_location_link_left');
			                         $footerLocationLinkRight  = get_sub_field('footer_location_link_right');
			                        ?>	          
			                        	<a href="<?php echo $footerLocationLinkLeft ?>"><?php echo $footerLocationNameLeft; ?></a>									                        	
								<?php endwhile; ?>
		        			<?php endif; ?>
	        			</div>
	        			<div class="col-xs-6">
							<?php if( have_rows('footer_locations') ): ?>
			                    <?php while( have_rows('footer_locations') ): the_row();
			
			                        // vars
			                        $footerLocationNameLeft  = get_sub_field('footer_location_name_left');
			                        $footerLocationNameRight  = get_sub_field('footer_location_name_right');
			                        $footerLocationLinkLeft = get_sub_field('footer_location_link_left');
			                         $footerLocationLinkRight  = get_sub_field('footer_location_link_right');
			                        ?>	          
			                        	<a href="<?php echo $footerLocationLinkRight ?>"><?php echo $footerLocationNameRight; ?></a>											

								<?php endwhile; ?>
			        		<?php endif; ?>	
	        			</div>
	        		</div>
	        	</div>        		
	        </div>			
			<div class="col-xs-12 col-sm-5 col-md-pull-1">
				<h4 class="footer-header"><?php the_field('footer_header'); ?></h4>
				<hr>
				<p class="footer-description"><?php the_field('footer_description'); ?></p>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-pull-9">
				<img class="img-responsive img-center nudge-top-xs" src="<?php the_field('footer_logo'); ?>">
			</div>					
		</div>
	</div>
</div>

<?php wp_footer(); ?>

</body>
</html>

<?php wp_footer(); ?>

</body>
</html>