<?php
/**
 * Template Name: Contact Page
 *
 * @package small-business-theme
 *
 */

get_header(); 

get_template_part( 'templates/hero-section' );
get_template_part('templates/contact-page');

?>

<?php get_footer(); ?>